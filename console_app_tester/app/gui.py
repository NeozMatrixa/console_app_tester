import re
import sys
import csv
import os
import ctypes
from datetime import datetime
from subprocess import PIPE, run
from PyQt5 import QtWidgets as Qw, QtGui
from PyQt5.QtWidgets import QTableWidgetItem


class TesterMainWindow(Qw.QMainWindow):
    """Main window class"""

    def __init__(self):
        """Private method - keep it low, take it slow"""
        super(TesterMainWindow, self).__init__()
        self.__set_window_properties()
        self.__menu_bar()
        self.__central_widget()
        self.__up_h_widget()
        self.__down_v_widget()
        self.__status_widget()
        self.__button_widget()
        self.__logger_and_status()
        self.__button()
        self.__sheet_table()

    def __set_window_properties(self):
        """Visual window GUI properties"""
        self.setWindowTitle("Console App Tester [ Pre-alpha 0.1 ]")
        self.resize(800, 600)
        self.setWindowIcon(QtGui.QIcon("icon.jpg"))

    def __menu_bar(self):
        menu_bar = self.menuBar()
        """Menu bar actions"""
        menu_file = menu_bar.addMenu("File")
        menu_help = menu_bar.addMenu("Help")

        """MenuBar File actions"""
        file_test = Qw.QAction('Run test', self)
        file_test.triggered.connect(self.__test_console_app)
        menu_file.addAction(file_test)

        file_import = Qw.QAction('Import data', self)
        file_import.triggered.connect(self.__select_sheet_dialog)
        menu_file.addAction(file_import)

        file_export = Qw.QAction('Export data', self)
        file_export.triggered.connect(self.__save_sheet_dialog)
        menu_file.addAction(file_export)

        file_exit = Qw.QAction('Exit', self)
        file_exit.triggered.connect(self.closeEvent)
        menu_file.addAction(file_exit)

        help_about = Qw.QAction('About', parent=menu_file)
        help_about.triggered.connect(self.about)
        menu_help.addAction(help_about)

        help_help = Qw.QAction('Help', parent=menu_file)
        help_help.triggered.connect(self.help)
        menu_help.addAction(help_help)

    def __central_widget(self):
        """Central layout (full window) widget method"""
        central_widget = Qw.QWidget(parent=self)
        self.setCentralWidget(central_widget)
        self.central_layout = Qw.QVBoxLayout(central_widget)
        central_widget.setLayout(self.central_layout)

    def __up_h_widget(self):
        """This layout takes upper half of central layout and cuts it horizontally in half """
        self.up_h_layout = Qw.QHBoxLayout(self)
        self.central_layout.addLayout(self.up_h_layout)

    def __down_v_widget(self):
        """This layout divides lower central layout for couple vertical spaces"""
        self.down_v_layout = Qw.QVBoxLayout(self)
        self.central_layout.addLayout(self.down_v_layout)

    def __status_widget(self):
        """Status layout on very bottom"""
        self.status_layout = Qw.QHBoxLayout(self)
        self.central_layout.addLayout(self.status_layout)

    def __button_widget(self):
        """Button layout left up side of window"""
        self.button_layout = Qw.QGridLayout(self)
        self.button_layout.setColumnStretch(1, 1)

        self.up_h_layout.addLayout(self.button_layout)

    def __logger_and_status(self):
        """Logger GUI"""
        self.logger = Qw.QTextEdit(parent=self)
        self.logger.setReadOnly(True)
        logger_label = Qw.QLabel("Logger")
        self.down_v_layout.addWidget(logger_label)
        self.down_v_layout.addWidget(self.logger)
        """Status GUI"""
        self.status = Qw.QLineEdit(parent=self)
        status_label = Qw.QLabel("Status: ")
        self.status_layout.addWidget(status_label)
        self.status.setReadOnly(True)
        self.status.setStyleSheet("QLineEdit { border: none }")
        self.status_layout.addWidget(self.status)
        self.status.setText("Hello")

    def __sheet_table(self):
        """Creates table for spreadsheet"""
        sheet_table = Qw.QTableWidget
        self.sheet = sheet_table(0, 5)
        col_headers = ['Program', 'Input', 'Expected output', 'Output', 'Path']
        self.sheet.setHorizontalHeaderLabels(col_headers)
        self.sheet.setEditTriggers(Qw.QAbstractItemView.NoEditTriggers)
        self.sheet.setSelectionBehavior(Qw.QAbstractItemView.SelectItems)
        self.up_h_layout.addWidget(self.sheet)
        self.sheet.resizeColumnToContents(0)
        self.sheet.resizeColumnToContents(1)
        self.sheet.resizeColumnToContents(2)
        self.sheet.resizeColumnToContents(3)
        self.sheet.resizeColumnToContents(4)

    def __button(self):
        """Keep all buttons here"""
        select_button = Qw.QPushButton("Manual test")
        select_button.clicked.connect(self.__manual_testing)

        import_csv_button = Qw.QPushButton("Import data")
        import_csv_button.clicked.connect(self.__select_sheet_dialog)

        save_csv_button = Qw.QPushButton("Generate report")
        save_csv_button.clicked.connect(self.__save_sheet_dialog)

        test_button = Qw.QPushButton("Test")
        test_button.clicked.connect(self.__test_console_app)

        self.debug_mode = Qw.QCheckBox("Debug mode")
        self.debug_mode.setChecked(False)
        self.status_layout.addWidget(self.debug_mode)

        self.numeric_mode = Qw.QCheckBox("Test numeric only")
        self.numeric_mode.setChecked(True)
        self.status_layout.addWidget(self.numeric_mode)

        self.button_layout.addWidget(import_csv_button)
        self.button_layout.addWidget(test_button)
        self.button_layout.addWidget(save_csv_button)
        self.button_layout.addWidget(select_button)

    def __select_file_dialog(self):
        """Opens CSV file - CSV(MS-DOS) ONLY HERE !!!!!!!!!"""
        self.status.setText("Selecting file")

        options_read = Qw.QFileDialog.Options()
        options_read |= Qw.QFileDialog.DontUseNativeDialog
        file_path, _ = Qw.QFileDialog.getOpenFileName(self, "Select python application", os.getenv('HOME'),
                                                      "Python (*.py)", options=options_read)
        if file_path:
            row = self.sheet.rowCount()
            self.sheet.insertRow(row)
            self.sheet.setItem(row, 0, QTableWidgetItem(os.path.basename(file_path)))
            self.sheet.setItem(row, 4, QTableWidgetItem(
                f'\{os.path.basename(os.path.dirname(file_path))}\{os.path.basename(file_path)}'))  # Must be \
            self.sheet.resizeColumnToContents(0)
            self.sheet.setItem(row, 1, QTableWidgetItem("0"))
            self.sheet.setItem(row, 2, QTableWidgetItem("0"))
            self.sheet.resizeColumnToContents(4)
            self.selected_file.setText(os.path.basename(file_path))
            self.logger.append(
                f"\n[{datetime.now().strftime('%H:%M:%S')}] {os.path.basename(file_path)} selected for manual testing")
            self.status.setText(f"{os.path.basename(file_path)} has loaded")

    def __select_sheet_dialog(self):
        """Opens CSV file - CSV(MS-DOS) ONLY HERE !!!!!!!!!"""
        self.status.setText("Select file to import")

        options_read = Qw.QFileDialog.Options()
        options_read |= Qw.QFileDialog.DontUseNativeDialog
        file_path, _ = Qw.QFileDialog.getOpenFileName(self, "Select data file", os.getenv('HOME'), "CSV (*.csv)",
                                                      options=options_read)
        if file_path:
            try:
                with open(file_path, newline='') as csv_file:

                    self.sheet.setRowCount(0)
                    my_file = csv.reader(csv_file, delimiter=';', quotechar='"')
                    for row_data in my_file:
                        row = self.sheet.rowCount()
                        self.sheet.insertRow(row)
                        if len(row_data) >= 3:
                            self.sheet.setColumnCount(len(row_data))
                            for column, stuff in enumerate(row_data):
                                item = QTableWidgetItem(stuff)
                                self.sheet.setItem(row, column, item)
                                self.sheet.resizeColumnToContents(column)
                self.logger.append(
                    f"\n[{datetime.now().strftime('%H:%M:%S')}] {os.path.basename(file_path)} import success")
                self.status.setText("Import success")
            except Exception:
                ctypes.windll.user32.MessageBoxW(0, "Can't open file.", "Error!", 16)

    def __save_sheet_dialog(self):
        """Save CSV file dialog"""
        self.status.setText("Saving data to CSV")

        file_path = Qw.QFileDialog.getSaveFileName(self, "Generate report", os.getenv('HOME'), "CSV(*.csv)")
        if file_path:
            try:
                with open(file_path[0], 'w', newline='') as csv_file:
                    writer = csv.writer(csv_file, delimiter=';', quotechar='"')
                    for row in range(self.sheet.rowCount()):
                        row_data = []
                        for column in range(self.sheet.columnCount()):
                            item = self.sheet.item(row, column)
                            if item is not None:
                                row_data.append(item.text())
                            else:
                                row_data.append('')
                        writer.writerow(row_data)
                self.logger.append(f"\n[{datetime.now().strftime('%H:%M:%S')}] File saved in ({file_path[0]})")
                self.status.setText("Saved")
            except Exception:
                self.logger.append(f"\n[{datetime.now().strftime('%H:%M:%S')}] Generating report aborted")
                self.status.setText("Operation aborted")
                ctypes.windll.user32.MessageBoxW(0, "File not saved.", "Warning!", 16)

    def __manual_testing(self):
        """About me - help"""
        self.status.setText("Manual testing")

        manual_test = Qw.QDialog(self)
        manual_test.setModal(True)
        manual_test.resize(400, 500)
        manual_test.setWindowTitle("Manual test")

        manual_test_widget = Qw.QVBoxLayout()
        manual_test.setLayout(manual_test_widget)
        manual_test_widget_up = Qw.QVBoxLayout()
        manual_test_widget.addLayout(manual_test_widget_up)
        manual_test_widget_down = Qw.QVBoxLayout()
        manual_test_widget.addLayout(manual_test_widget_down)

        label_file = Qw.QLabel("Selected file:")
        manual_test_widget_up.addWidget(label_file)

        self.selected_file = Qw.QLineEdit(" None ")
        self.selected_file.setReadOnly(True)
        self.selected_file.setStyleSheet("QLineEdit { border: none }")
        manual_test_widget_up.addWidget(self.selected_file)

        select_file_button = Qw.QPushButton("Select file")
        select_file_button.clicked.connect(self.__select_file_dialog)
        manual_test_widget_up.addWidget(select_file_button)

        label_input = Qw.QLabel("Type input")
        manual_test_widget_up.addWidget(label_input)

        self.man_input = Qw.QPlainTextEdit("0")
        manual_test_widget_up.addWidget(self.man_input)

        label_output = Qw.QLabel("Type expected output")
        manual_test_widget_up.addWidget(label_output)

        self.man_ex_output = Qw.QPlainTextEdit("0")
        manual_test_widget_up.addWidget(self.man_ex_output)

        test_button = Qw.QPushButton("Test")
        manual_test_widget_down.addWidget(test_button)
        test_button.clicked.connect(self.__manual_test_console_app)
        test_button.clicked.connect(manual_test.reject)
        manual_test.exec_()

    def __manual_test_console_app(self):
        if self.sheet.rowCount() <= 0:
            ctypes.windll.user32.MessageBoxW(0, "File not selected.", "Warning!", 16)
            self.status.setText("Error")
        else:
            self.sheet.setItem(self.sheet.rowCount() - 1, 1, QTableWidgetItem(self.man_input.toPlainText()))
            self.sheet.setItem(self.sheet.rowCount() - 1, 2, QTableWidgetItem(self.man_ex_output.toPlainText()))
            self.sheet.resizeColumnToContents(1)
            self.sheet.resizeColumnToContents(2)
            self.logger.append(f"\n[{datetime.now().strftime('%H:%M:%S')}] Running test in manual mode...")
            self.__test_console_app()

    def __test_console_app(self):
        """Here is the heart of testing core"""
        if self.sheet.rowCount() <= 0:
            ctypes.windll.user32.MessageBoxW(0, "No application to test. Import data or run manual test.", "Warning!",
                                             16)
        else:
            self.status.setText("Running test")

            for row_data in range(self.sheet.rowCount()):
                var = str(
                    f'{os.path.dirname(os.path.dirname(os.path.realpath(__file__)))}{self.sheet.item(row_data, 4).text()}')
                self.logger.append(
                    f"\n[{datetime.now().strftime('%H:%M:%S')}] Testing -> {self.sheet.item(row_data, 0).text()}")
                number = self.sheet.item(row_data, 1).text()
                result = run(f'python {var}', input=number, stdout=PIPE, stderr=PIPE, universal_newlines=True,
                             shell=True)
                if self.numeric_mode.isChecked() is True:
                    output = re.sub('[^0-9-.]', '', result.stdout)  # [^0-9-.] this is very important
                else:
                    output = result.stdout

                if self.debug_mode.isChecked() is False:
                    self.logger.append(f"[{datetime.now().strftime('%H:%M:%S')}] Input: {number}")
                    self.logger.append(f"[{datetime.now().strftime('%H:%M:%S')}] Output:  {output}")
                    self.logger.append(
                        f"[{datetime.now().strftime('%H:%M:%S')}] No errors found") if result.stderr == '' else self.logger.append(
                        f"[{datetime.now().strftime('%H:%M:%S')}] Errors: {result.stderr}\n")
                    self.sheet.setItem(row_data, 3, QTableWidgetItem(output))
                    self.compare_output(row_data)
                else:
                    self.logger.append(f"[{datetime.now().strftime('%H:%M:%S')}] {result}")
                self.status.setText("Test completed")

    def compare_output(self, row):
        if self.sheet.item(row, 2).text() != self.sheet.item(row, 3).text():
            self.sheet.item(row, 3).setBackground(QtGui.QColor(200, 70, 70))
            self.logger.append(
                f"[{datetime.now().strftime('%H:%M:%S')}]\t \t \t       !!!Output in {row + 1} row is not equal to expected output!!!")
        else:
            self.sheet.item(row, 3).setBackground(QtGui.QColor(70, 200, 70))

    def help(self):
        """About me - help"""
        help_me = Qw.QDialog(self)
        help_me.setModal(True)
        help_me.resize(500, 300)
        help_me.setWindowTitle("Help")

        help_me_widget = Qw.QVBoxLayout()
        help_me.setLayout(help_me_widget)

        label = Qw.QLabel("Welcome in Console App Tester")
        help_me_widget.addWidget(label)

        help_text = Qw.QPlainTextEdit()
        help_text.setReadOnly(True)
        help_text.setStyleSheet("QLineEdit { border: none }")
        help_text.appendPlainText('Couple things you should know:')
        help_text.appendPlainText('    1. Testing apps MUST be located in "sample_tests" directory.')
        help_text.appendPlainText('    2. In debug mode table is disabled to prevent providing corrupted data')
        help_text.appendPlainText('    3. When first start application, the Test numeric only checkbox i checked.')
        help_text.appendPlainText(
            '       This mode is only for numeric tests. If you want test string you must uncheck this option')
        help_text.appendPlainText('    4. CSV file to import MUST be like:')
        help_text.appendPlainText('your_app_name.py;input;expected_output;;\sample_tests\your_app_name.py')
        help_me_widget.addWidget(help_text)

        help_me.exec_()

    def about(self):
        try:
            os.startfile('https://gitlab.com/NeozMatrixa/console_app_tester/-/blob/master/README.md')
        except OSError:
            self.logger.append(
                f"[{datetime.now().strftime('%H:%M:%S')}] Please open a browser on: https://gitlab.com/NeozMatrixa/console_app_tester/-/blob/master/README.md")

    def closeEvent(self, event):
        """Exit from application"""
        self.status.setText("Goodbye")
        quit_msg = "Are you sure you want to exit Console App Tester?"
        reply = Qw.QMessageBox.question(self, 'Confirm Exit', quit_msg, Qw.QMessageBox.Yes, Qw.QMessageBox.No)
        if reply == Qw.QMessageBox.Yes:
            try:
                event.accept()
            finally:
                sys.exit(0)

        else:
            if not type(event) == bool:
                event.ignore()
            else:
                pass
