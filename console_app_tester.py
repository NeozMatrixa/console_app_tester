from console_app_tester.app.gui import TesterMainWindow
from PyQt5 import QtWidgets as Qw
import sys


class ConsoleAppTester:
    app = Qw.QApplication(sys.argv)
    tester_window = TesterMainWindow()
    tester_window.show()
    app.exec_()
