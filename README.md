# Console App Tester - GUI version
This application tests console python programs using data from CSV file

## How to use it
1. Put your testing console python program in the sample_tests folder.
2. Put your CSV spreadsheet in the sample_tests folder <additionally>.
3. Run console_app_tester.py.
4. Click manual test if you have only .py console program or click Import data to import spreadsheet.
5. If you imported data then just click Test (remember to check test mode in bottom right corner).
6. If you clicked manual test then select your app (remember to always put your testing apps in sample_tests folder), type input, output and run test.
7. More informations you can find in help


### **About app**
#### App takes input from table and use it in tested app.
![alt text][c1]
#### It compare tested application output with expected in table.
![alt text][c2]
#### Console App Tester is also equipped with debug mode.
![alt text][c3]
#### Program have manual test mode 
![alt text][c4]
 

## Authors

![alt text][logo]
**Radosław Jachimowicz**
========================


## Acknowledgments

*  [marx](https://forums.frontier.co.uk/threads/list-of-earth-like-worlds-v2.168287/)
*  [Draqun](https://github.com/Draqun/python_good_practices)
*  [PurpleBooth](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2)
*  [adam-p](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)


[logo]: https://avatars1.githubusercontent.com/u/48450331?s=100&amp;u=0f1303f8e3ddfbd6746c961de2f8a644ca50ada0&amp;v=4 "Siemanko :)"
[c1]:  https://gitlab.com/NeozMatrixa/console_app_tester/-/raw/master/readme_pictures/cat1.PNG "App just started"
[c2]:  https://gitlab.com/NeozMatrixa/console_app_tester/-/raw/master/readme_pictures/cat2.PNG "Testing"
[c3]:  https://gitlab.com/NeozMatrixa/console_app_tester/-/raw/master/readme_pictures/cat3.PNG "Debug"
[c4]:  https://gitlab.com/NeozMatrixa/console_app_tester/-/raw/master/readme_pictures/cat4.PNG "Manual mode"